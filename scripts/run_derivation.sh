#!/bin/bash

source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup AthDerivation,21.2,latest,here

cd $1
Reco_tf.py --inputEVNTFile $1.EVNT.root --outputDAODFile $1.pool.root --reductionConf TRUTH3
cd ../
